//
//  NewStoryViewController.h
//  Links
//
//  Created by Michael Thompson on 2015-11-14.
//  Copyright © 2015 Michael Thompson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewStoryViewController : UIViewController <UITextFieldDelegate>

@end
