//
//  StoriesTableViewController.m
//  Links
//
//  Created by Michael Thompson on 2015-11-14.
//  Copyright © 2015 Michael Thompson. All rights reserved.
//

#import "StoriesTableViewController.h"
#import "StoryManager.h"
#import "SnippetViewController.h"
#import <Parse/Parse.h>
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIScrollView+SVInfiniteScrolling.h"

@interface StoriesTableViewController () {
    NSMutableArray *tableData;
    StoryManager *storyManager;
}

@end

@implementation StoriesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"--- StoriesTableViewController");
    storyManager = [StoryManager sharedStoryManager];
    tableData = [[NSMutableArray alloc] init];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Desperately loading stories...";

    [self fetchStories];
    
    self.navigationItem.title = @"Stories";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)fetchStories {
    [storyManager fetchStoriesWithCompletion:^(NSArray *results) {
        if (results != nil && results.count > 0) {
            [tableData removeAllObjects];
            for (PFObject *object in results) {
                [tableData addObject:object];
            }
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.tableView reloadData];
        } else {
            
        }
    }];
}



#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return tableData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StoryCell" forIndexPath:indexPath];
    
    // Configure the cell...
    PFObject *story = [tableData objectAtIndex:indexPath.row];
    cell.textLabel.text = [story objectForKey:@"name"];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    
    if (section == 0) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSString *url = [userDefaults objectForKey:@"profilePicUrl"];
        NSString *name = [userDefaults objectForKey:@"name"];
        
        UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:10];
        UILabel *nameLabel = (UILabel *)[cell.contentView viewWithTag:20];

        
        NSURL *profilePicUrl = [NSURL URLWithString:url];
        [imageView sd_setImageWithURL:profilePicUrl placeholderImage:[UIImage imageNamed:@"grump.png"]];
        
        CGFloat width = imageView.layer.frame.size.width;
        imageView.layer.cornerRadius = width/2.0f;
        imageView.layer.masksToBounds = YES;
        imageView.layer.borderWidth = 5.0f;
        imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
        
        nameLabel.text = name;
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 80;
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"NewSnippetSegue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        SnippetViewController *snippetViewController = segue.destinationViewController;
        snippetViewController.storyId = [tableData objectAtIndex:indexPath.row];
    }
}


- (IBAction)unwindFromNewStoryView:(UIStoryboardSegue *)segue {
    NSLog(@"---- refresh list");
    [self fetchStories];

}

- (IBAction)unwindFromNewSnippetView:(UIStoryboardSegue *)segue {
    [self fetchStories];

}

@end
