//
//  StoryManager.m
//  Links
//
//  Created by Michael Thompson on 2015-11-14.
//  Copyright © 2015 Michael Thompson. All rights reserved.
//

#import "StoryManager.h"
#import <Parse/Parse.h>

@implementation StoryManager

+ (StoryManager *)sharedStoryManager {
    static StoryManager *storyManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        storyManager = [[self alloc]init];
    });
    
    return storyManager;
}

- (void)fetchStoriesWithCompletion:(void(^)(NSArray *results))callback {
    
    PFQuery *query = [PFQuery queryWithClassName:@"Story"];
    [query orderByDescending:@"createdAt"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSLog(@"Successfully retrieved %ld STORIES.", objects.count);
            
            NSArray *results = [[NSArray alloc] initWithArray:objects];
            callback(results);
            
        } else {
            NSLog(@"----- error when querying Parse for custom products");
            callback(nil);
        }
    }];
}


- (void)fetchSnippetsForStoryId:(NSString *)storyId withCompletion:(void(^)(NSArray *results))callback {
    PFQuery *query = [PFQuery queryWithClassName:@"Snippet"];
    [query whereKey:@"ohjectId" equalTo:storyId];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSLog(@"Successfully retrieved %ld SNIPPETS.", objects.count);
            
            NSArray *results = [[NSArray alloc] initWithArray:objects];
            callback(results);
            
        } else {
            NSLog(@"----- error when querying Parse for custom products");
            callback(nil);
        }
    }];
}


- (void)addStory:(NSString *)name withCompletion:(void(^)(bool success, NSString *newId))callback {
    PFObject *newStory = [PFObject objectWithClassName:@"Story"];
    NSString *username = [PFUser currentUser].username;
    if (username == nil || username.length == 0) {
        username = @"tester";
    }
    newStory[@"creator"] = username;
    newStory[@"name"] = name;
    newStory[@"snippets"] = [[NSMutableArray alloc] init];
    
    [newStory saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (succeeded) {
            callback(YES, newStory.objectId );
        } else {
            callback(NO, nil);
        }
    }];
}

- (void)addSnippet:(NSString *)caption withImage:(UIImage *)image toStoryId:(NSString *)storyId withCompletion:(void(^)(bool success))callback {
    PFObject *newSnippet = [PFObject objectWithClassName:@"Snippet"];
    newSnippet[@"caption"] = caption;
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5f);
    PFFile *imageFile = [PFFile fileWithName:@"image.jpg" data:imageData];
    newSnippet[@"media"] = imageFile;
    newSnippet[@"imageUrl"] = @"";
 
    [newSnippet saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (succeeded) {

                PFFile *theFile = newSnippet[@"media"];
                NSString *theImageUrl = theFile.url;
                newSnippet[@"imageUrl"] = theImageUrl;
                [newSnippet saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                    if (succeeded) {
                        NSString *snippetId = newSnippet.objectId;
                        [self addSnippet:snippetId toStoryId:storyId withCompletion:^(bool success) {
                            if (success) {
                                callback(YES);
                            } else {
                                callback(NO);
                            }
                        }];
                    } else {
                        callback(NO);
                    }
                }];

        } else {
            callback(NO);
        }
    }];
}

- (void)addSnippet:(NSString *)snippetId toStoryId:(NSString *)storyId withCompletion:(void(^)(bool success))callback {
    PFQuery *query = [PFQuery queryWithClassName:@"Story"];
    [query whereKey:@"objectId" equalTo:storyId];
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable story, NSError * _Nullable error) {
        
        NSMutableArray *snippets = [story objectForKey:@"snippets"];
        [snippets addObject:snippetId];
        [story saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            if (succeeded) {
                callback(YES);
            } else {
                callback(NO);
            }
        }];
    }];
}






@end
