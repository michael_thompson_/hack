//
//  StoryManager.h
//  Links
//
//  Created by Michael Thompson on 2015-11-14.
//  Copyright © 2015 Michael Thompson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface StoryManager : NSObject

+ (StoryManager *)sharedStoryManager;

- (void)addStory:(NSString *)name withCompletion:(void(^)(bool success, NSString *newStoryId))callback;
- (void)addSnippet:(NSString *)caption withImage:(UIImage *)image toStoryId:(NSString *)storyId withCompletion:(void(^)(bool success))callback;

- (void)fetchStoriesWithCompletion:(void(^)(NSArray *results))callback;
- (void)fetchSnippetsForStoryId:(NSString *)storyId withCompletion:(void(^)(NSArray *results))callback;

@end
