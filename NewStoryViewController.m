//
//  NewStoryViewController.m
//  Links
//
//  Created by Michael Thompson on 2015-11-14.
//  Copyright © 2015 Michael Thompson. All rights reserved.
//

#import "NewStoryViewController.h"
#import "StoryManager.h"
#import "MBProgressHUD.h"

@interface NewStoryViewController () {
    UIImagePickerController *imagePickerController;
    BOOL picWasTaken;
    UITextField *activeTextField;
    StoryManager *storyManager;
    
}

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextField *storyNameTextField;

@end

@implementation NewStoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.title = @"Create New Story!";
    storyManager = [StoryManager sharedStoryManager];

    picWasTaken = NO;
    imagePickerController = [[UIImagePickerController alloc] init];
    self.storyNameTextField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleKeyboardDidShow:)
     name:UIKeyboardDidShowNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillBeHidden:)
     name:UIKeyboardWillHideNotification
     object:nil];

}

- (void)dealloc {
    [self unsubscribeFromKeyboardNotifications];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (! picWasTaken) {
        [self takePicture];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Save button

- (IBAction)saveButtonTapped:(id)sender {
    NSLog(@"---- save this new story and snippet!");
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Cross your fingers...";
    
    __unsafe_unretained typeof(self) weakSelf = self;
    [storyManager addStory:activeTextField.text withCompletion:^(bool success, NSString *newId) {
        if (success) {
            NSLog(@"---- saved story: %@", newId);
            
            [storyManager addSnippet:@"test snippet" withImage:weakSelf.imageView.image toStoryId:newId withCompletion:^(bool success) {
                NSLog(@"---- saved snippet for new Story!");
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [weakSelf performSegueWithIdentifier:@"unwindToStoriesView" sender:weakSelf];
            }];
        } else {
           NSLog(@"---- error creating new Story!");
           [MBProgressHUD hideHUDForView:self.view animated:YES];
           [weakSelf performSegueWithIdentifier:@"unwindToStoriesView" sender:weakSelf];
        }
    }];
}


#pragma mark - TakePicture

- (void)takePicture {
    
#if TARGET_IPHONE_SIMULATOR
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
#else
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
#endif
    imagePickerController.editing = NO;
    imagePickerController.showsCameraControls = YES;
    imagePickerController.delegate = (id)self;
    
    picWasTaken = YES;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}



#pragma mark - ImagePicker methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *photoImage = [info objectForKeyedSubscript:UIImagePickerControllerOriginalImage];
    [self.imageView setImage:photoImage];
    
    NSLog(@"----- setting the image with the photo image");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    NSLog(@"----- cancel");
    [self performSegueWithIdentifier:@"unwindToStoriesView" sender:self];
}



#pragma mark - UITextFieldDelegate 

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    activeTextField = textField;
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    activeTextField = textField;
    NSLog(@"***** %@", textField.text);
    
    [self hideKeyboard];
}


- (void)hideKeyboard {
    NSArray *subviews = [self.view subviews];
    for (id objects in subviews) {
        if ([objects isKindOfClass:[UITextField class]]) {
            UITextField *theTextField = objects;
            if ([objects isFirstResponder]) {
                [theTextField resignFirstResponder];
            }
        }
    }
}



#pragma mark - Keyboard handling

- (void)handleTap:(UIGestureRecognizer *)recognizer {
    if ([self.storyNameTextField isFirstResponder]) {
        [self.storyNameTextField resignFirstResponder];
    }
}

// This method will be called when the user touches on the tableView, at
// which point we will hide the keyboard (if open). This method is called
// because UITouchTableView.m calls nextResponder in its touch handler.
- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event {
    if ([self.storyNameTextField isFirstResponder]) {
        [self.storyNameTextField resignFirstResponder];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)notification {
    NSLog(@"--- keyboardWillBeHidden method.");
    
    NSDictionary *userInfo = [notification userInfo];
    
    CGRect keyboardEndFrame;
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey]
     getValue:&keyboardEndFrame];
    
    UIViewAnimationCurve animationCurve;
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey]
     getValue:&animationCurve];
    
    NSTimeInterval animationDuration;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey]
     getValue:&animationDuration];
    
    // Get the correct keyboard size to we slide the right amount.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    int y = keyboardFrame.size.height;    //(up ? -1 : 1);
    self.view.frame = CGRectOffset(self.view.frame, 0, y);
    
    [UIView commitAnimations];
}


- (void) handleKeyboardDidShow:(NSNotification *)notification {
    NSLog(@"--- handleKeyboardDidShow method.");
    NSDictionary *userInfo = [notification userInfo];
    
    CGRect keyboardEndFrame;
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey]
     getValue:&keyboardEndFrame];
    
    UIViewAnimationCurve animationCurve;
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey]
     getValue:&animationCurve];
    
    NSTimeInterval animationDuration;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey]
     getValue:&animationDuration];
    
    // Get the correct keyboard size to we slide the right amount.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    int y = keyboardFrame.size.height * -1;    //(up ? -1 : 1);
    self.view.frame = CGRectOffset(self.view.frame, 0, y);
    
    [UIView commitAnimations];
}

- (void)unsubscribeFromKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification object:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification object:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
