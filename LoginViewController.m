//
//  LoginViewController.m
//  Links
//
//  Created by Michael Thompson on 2015-11-14.
//  Copyright © 2015 Michael Thompson. All rights reserved.
//

#import "LoginViewController.h"
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"--- LoginViewController");
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)loginButtonTapped:(id)sender {
    
    if (!FBSession.activeSession.isOpen) {
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"user_friends"] allowLoginUI:YES fromViewController:self completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            if (error) {
                NSLog(@"----- FB issue.");
            } else {
                [self getUserInfo];
            }
        }];
         
    } else {
        [self getUserInfo];
    }
}


- (void)getUserInfo {
    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            // Success! Include your code to handle the results here
            NSLog(@"user info: %@", result);
            
            NSString *url = @"https://graph.facebook.com/";
            url = [url stringByAppendingString:[result objectForKey:@"id"]];
            url = [url stringByAppendingString:@"/picture?height=240&width=240"];
            NSLog(@"FB profile pic Url: %@", url);
            
            // save user info
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:[result objectForKey:@"id"] forKey:@"facebookId"];
            [userDefaults setValue:[result objectForKey:@"first_name"] forKey:@"firstName"];
            [userDefaults setValue:[result objectForKey:@"last_name"] forKey:@"lastName"];
            [userDefaults setValue:[result objectForKey:@"name"] forKey:@"name"];
            [userDefaults setValue:url forKey:@"profilePicUrl"];
            [userDefaults setValue:@"" forKey:@"username"];
            [userDefaults synchronize];
            
            [[PFUser currentUser] setObject:[result objectForKey:@"id"] forKey:@"facebookId"];
            [[PFUser currentUser] setObject:[result objectForKey:@"name"] forKey:@"name"];
            
            if (url != nil && [url length] != 0) {
                [[PFUser currentUser] setObject:url forKey:@"profilePicUrl"];
            }
            
            [[PFUser currentUser] saveInBackground];
            
//            PFInstallation *currentInstallation = [PFInstallation currentInstallation];
//            [currentInstallation setDeviceTokenFromData:[userDefaults objectForKey:@"deviceToken"]];
//            [currentInstallation setObject:[result objectForKey:@"id"] forKey:@"facebookId"];
//            [currentInstallation saveInBackground];
            
            [self performSegueWithIdentifier:@"MainSegue" sender:self];
        } else {
 
        }
    }];
}



@end
