//
//  SnippetViewController.h
//  Links
//
//  Created by Michael Thompson on 2015-11-14.
//  Copyright © 2015 Michael Thompson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SnippetViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) NSString *storyId;

@end
